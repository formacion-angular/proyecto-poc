import { Http, HttpModule, Response, ResponseOptions } from '@angular/http';
import { AuthenticationService } from './authentication.service';
import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { MockBackend } from '@angular/http/testing';

describe('AuthenticationService', () => {

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [],
            imports: [HttpModule],
            providers: [AuthenticationService, MockBackend]
        })
    }));

    it('should login', inject([AuthenticationService, MockBackend], (authenticationService: AuthenticationService, mockBackend: MockBackend) => {

        const USERNAME: string = 'pedro';
        const PASSWORD: string = '123456';

        const MOCK_RESPONSE = {
            "success": true,
            "message": "Enjoy your token!",
            "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJwZWRybyIsImlhdCI6MTQ4OTc1MDc1MSwiZXhwIjogMjYxOTg4NTg0NzN9.0oNYteNgreNt6ljSKnYl_99PYOJVEdKpFQY9bJiUDuQ"
        };

        mockBackend.connections.subscribe((connection) => {
            connection.mockRespond(new Response(new ResponseOptions(
                { body: JSON.stringify(MOCK_RESPONSE) }
            )));
        });

        authenticationService.login(USERNAME, PASSWORD).subscribe(
            (response) => { expect(response).toBe(true) }
        );

    }));

    it('should not login', inject([AuthenticationService, MockBackend], (authenticationService: AuthenticationService, mockBackend: MockBackend) => {

        const USERNAME: string = 'pedro123';
        const PASSWORD: string = '123456';

        const MOCK_RESPONSE = {
            "success": false,
            "message": "Authentication failed. User not found."
        };

        mockBackend.connections.subscribe((connection) => {
            connection.mockRespond(new Response(new ResponseOptions(
                { body: JSON.stringify(MOCK_RESPONSE) }
            )));
        });

        authenticationService.login(USERNAME, PASSWORD).subscribe(
            (response) => { expect(response).toBe(false) }
        );

    }));

    it('should logout', inject([AuthenticationService], (authenticationService: AuthenticationService) => {
        const TOKEN: string = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJwZWRybyIsImlhdCI6MTQ4OTc1MDc1MSwiZXhwIjogMjYxOTg4NTg0NzN9.0oNYteNgreNt6ljSKnYl_99PYOJVEdKpFQY9bJiUDuQ";
        authenticationService.token = TOKEN;
        authenticationService.logout();
        expect(authenticationService.token).toBeNull();
    }));

});