import { Profile } from './../../profile/profile';
import { API_DOMAIN } from './../../app.config';
import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class UserService {

    constructor(private http: Http) {}

    getUserProfile(): Observable<Profile> {
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', `Bearer ${currentUser.token}`);

        let options = new RequestOptions({ headers: headers });

        return this.http.get(API_DOMAIN + 'api/secure/myUserData', options)
            .map((response: Response) => {
                return response.json();
            });
    }

    getRoles(): Observable<string[]> {
        return this.http.get(API_DOMAIN + 'api/roles')
        .map((response: Response) => {
                return response.json();
        });
    }

    updateProfile(userProfile:Profile):Observable<boolean>{
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('Authorization', `Bearer ${currentUser.token}`);

        let options = new RequestOptions({ headers: headers });
        return this.http.post(API_DOMAIN + 'api/secure/updateMyUserData',userProfile,options).map(
            (response: Response) => {

                let responseFromServer = response.json();
                if(responseFromServer.username != undefined){
                return true;
            }
            return false;
        })

    }

}