import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

@Injectable()
export class HomeGuard implements CanActivate {

    constructor(private router: Router) { }

    canActivate() {
        let token: string = localStorage.getItem('currentUser');
        if (localStorage.getItem('currentUser')) {
            // logged in so return true
            let expiryDateMilis: number = this.__extractExpirationDate(token);
            let nowDate: Date = new Date();

            if (nowDate.getTime() < expiryDateMilis) {
                return true;
            }
        }

        this.router.navigate(['/login']);
        return false;
    }
    
    __extractExpirationDate(token: String) {
        let slices: string[] = token.split('.');
        let middleTokenSlice = slices[1];
        let expiryDateMilis: number;

        middleTokenSlice = window.atob(middleTokenSlice);

        try {
            let json = JSON.parse(middleTokenSlice);
            let seconds: number = parseInt(json.exp);
            expiryDateMilis = seconds * 1000;
        } catch (error) {
            expiryDateMilis = 0;
        }

        return expiryDateMilis;
    }
}