import { Routes, RouterModule } from "@angular/router";

import { ProfileComponent } from './profile/profile.component';
import { LoginComponent } from "./login/login.component";
import { HomeComponent } from "./home/home.component";
import { HomeGuard } from "./home/home.guard";

const APP_ROUTES: Routes = [
    {path: 'login', component: LoginComponent},
    {path: 'profile', component: ProfileComponent, canActivate: [HomeGuard]},
    {path: 'home', component: HomeComponent, canActivate: [HomeGuard]},

    {path:'**', redirectTo: 'home'}
];

export const routing = RouterModule.forRoot(APP_ROUTES);