import { Router } from '@angular/router';
import { AuthenticationService } from './../services/authenticate/authentication.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginModel: any = {};
  loading: boolean = false;
  mediaProLogo: string;
  error: string;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.mediaProLogo = "./assets/mediapro-color.png"
  }

  login() {
    this.loading = true;
    this.error = '';
    if (this.loginModel.username != undefined && this.loginModel.password != undefined && this.loginModel.username != '' && this.loginModel.password != '') {
      this.authenticationService.login(this.loginModel.username, this.loginModel.password)
        .subscribe(result => {
          if (result === true) {
            this.router.navigate(['/']);
          } else {
            this.error = 'Username or password is incorrect';
            this.loading = false;
          }
        });
    }
    this.loading = false;
  }

}