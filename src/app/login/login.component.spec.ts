import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthenticationService } from './../services/authenticate/authentication.service';

import { LoginComponent } from './login.component';
import { Observable } from 'rxjs';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoginComponent],
      imports: [FormsModule,
        HttpModule,
        RouterTestingModule.withRoutes(
          [{ path: 'login', component: LoginComponent }]
        )],
      providers: [AuthenticationService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call the AuthenticationService and show an error message', () => {
    const username = "pedro";
    const password = "123456";

    fixture.componentInstance.loginModel = { username, password };

    let authenticationService = fixture.debugElement.injector.get(AuthenticationService);
    spyOn(authenticationService, 'login').and.returnValue(Observable.of(false));

    fixture.componentInstance.login();

    expect(fixture.componentInstance.error).toMatch("Username or password is incorrect");
    expect(authenticationService.login).toHaveBeenCalled();
  });

});
