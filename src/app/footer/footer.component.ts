import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  logoliga: string;
  mediaProLogo: string;
  constructor() { }

  ngOnInit() {
    this.logoliga = "./assets/laligalogo.png";
    this.mediaProLogo = "./assets/mediapro.png"
  }

}
