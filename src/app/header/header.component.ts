import { Router } from '@angular/router';
import { AuthenticationService } from './../services/authenticate/authentication.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  brandImage: string;
  username: string;

  constructor(private router: Router, private authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.brandImage = "./assets/mediapro-login.png";
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));

    if (localStorage.getItem('currentUser')) {
      this.username = currentUser.username;
    } else {
      this.username = 'Username';
    }
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }

}
