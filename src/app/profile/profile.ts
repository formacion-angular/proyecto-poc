export class Profile {
    username: string;
    password: string;
    email: string;
    isSubscribed: boolean;
    role: string;
}