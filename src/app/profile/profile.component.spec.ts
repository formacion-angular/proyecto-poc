import { Observable } from 'rxjs';
import { AuthenticationService } from './../services/authenticate/authentication.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpModule } from '@angular/http';
import { FooterComponent } from './../footer/footer.component';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileComponent } from './profile.component';
import { HeaderComponent } from '../header/header.component';
import { HomeComponent } from '../home/home.component';
import { FormsModule } from '@angular/forms';
import { UserService } from '../services/user/user.service';
import { Profile } from './profile';

describe('ProfileComponent', () => {
  let component: ProfileComponent;
  let fixture: ComponentFixture<ProfileComponent>;
  let userService: UserService;

  const TOKEN: string = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJwZWRybyIsImlhdCI6MTQ4OTc1MDc1MSwiZXhwIjogMjYxOTg4NTg0NzN9.0oNYteNgreNt6ljSKnYl_99PYOJVEdKpFQY9bJiUDuQ";
  const USERNAME: string = "pedro";
  const ROLES = ["ADMIN", "VIP", "BASIC"];
  const CURRENT_USER: any = JSON.stringify({ username: USERNAME, token: TOKEN });
  const USER_PROFILE: Profile = { username: USERNAME, password: "123456", email: "pedro@fake.com", isSubscribed: true, role: "ADMIN" };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProfileComponent, HeaderComponent, FooterComponent, HomeComponent],
      imports: [FormsModule, HttpModule,
        RouterTestingModule.withRoutes(
          [{ path: 'home', component: HomeComponent }]
        )],
      providers: [UserService, AuthenticationService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    localStorage.setItem('currentUser', CURRENT_USER);

    fixture = TestBed.createComponent(ProfileComponent);
    component = fixture.componentInstance;
    userService = fixture.debugElement.injector.get(UserService);

    spyOn(userService, 'getUserProfile').and.returnValue(Observable.of(USER_PROFILE));
    spyOn(userService, 'getRoles').and.returnValue(Observable.of(ROLES));

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call the UserService and set true the updated message', () => {
    spyOn(userService, 'updateProfile').and.returnValue(Observable.of(true));
    fixture.componentInstance.update();

    expect(fixture.componentInstance.isUpdated).toBeTruthy();
    expect(userService.updateProfile).toHaveBeenCalled();
  });
});
