import { Component, OnInit } from '@angular/core';
import { API_DOMAIN } from '../app.config';
import { Profile } from './profile';
import { UserService } from '../services/user/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})

export class ProfileComponent implements OnInit {
  userData: Profile = new Profile();
  userRoles: string[] = [];
  isUpdated: boolean = false;

  constructor(private userService: UserService) { }

  ngOnInit() {

    this.userService.getRoles()
      .subscribe(result => {
        this.userRoles = result;
      });

    this.userService.getUserProfile()
      .subscribe(result => {
        this.userData = result;
      });
  }

  update() {
    this.userService.updateProfile(this.userData).subscribe(result => {
      this.isUpdated = result;
    });
  }
}
