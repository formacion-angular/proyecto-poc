import { MediaProPocPage } from './app.po';

describe('media-pro-poc App', () => {
  let page: MediaProPocPage;

  beforeEach(() => {
    page = new MediaProPocPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
